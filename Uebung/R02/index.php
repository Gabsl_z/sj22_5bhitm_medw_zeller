<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Recap 02</title>
</head>
<body>
<?php
# ------------------------------------------------
# Connect to Database
# ------------------------------------------------
$user = "htl";
$pw = "insy";
$dbname = "news";
try {
    $conn = new PDO("mysql:host=localhost;dbname=$dbname", $user, $pw);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM articles";
} catch (PDOException $e) {
    echo "<br>" . $e->getMessage();
}
?>
<div class="container">
    <h1 class="lead mt-5"><b>Aktuelle Beiträge</b></h1>
    <div class="row">
    <?php
    foreach ($conn->query($sql) as $row) { ?>
    <div class="card col-3 m-5">
        <div class="card-body">
            <img src="<?php echo $row['image'] ?>" class="card-img-top" alt="...">
            <h5 class="card-title">
                <?php
                    echo $row["title"];
                ?>
            </h5>
            <h6 class="card-subtitle mb-2 text-muted">
                <?php
                    echo $row["date"];
                ?>
            </h6>
            <p class="card-text">
                <?php
                    echo $row["description"];
                ?>
            </p>
        </div>
    </div>
    <?php
    }
    ?>
    </div>
</div>
</body>
</html>