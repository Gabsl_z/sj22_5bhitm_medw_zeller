<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <title>Recap 03</title>
</head>
<body>
<?php
# ------------------------------------------------
# Connect to Database
# ------------------------------------------------
$user = "htl";
$pw = "insy";
$dbname = "medrecap";
try {
    $conn = new PDO("mysql:host=localhost;dbname=$dbname", $user, $pw);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM energy";
} catch (PDOException $e) {
    echo "<br>" . $e->getMessage();
}
?>
<div class="container">
    <h1 class="lead mt-5"><b>Strompreisvergleich</b></h1>
    <div class="row">
        <div class="col-3">
            <form action="index.php" method="get">
                <div class="card-body">
                    <h5 class="form-label">kWh/Jahr</h5>
                    <input type="number" name="kwh" class="form-control" <?php if (isset($_GET["kwh"])) {
                        echo "value=\"" . $_GET["kwh"] . "\"";
                    } else echo "0"?>>
                </div>
                <div class="card-body">
                    <input type="submit" class="btn btn-lg btn-primary" value="Jetzt vergleichen">
                </div>
            </form>
        </div>
    </div>
    <?php
    if (isset($_GET["kwh"])) { ?>
        <h1 class="lead mt-5"><b>3 Angebote</b></h1>
        <div class="row">
            <table class="table" id="myTable1">
                <thead>
                <tr>
                    <th onclick="sortTable(0)">Anbieter</th>
                    <th onclick="sortTable(1)">Wichtige Information</th>
                    <th onclick="sortTable(2)">Jahrespreis</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach ($conn->query($sql) as $row) {

                    $kwh = $_GET["kwh"];
                    $annual_cost = 0;

                    switch ($kwh) {
                        case $kwh <= 1000:
                            $annual_cost = $kwh * $row["price_1"];
                            break;
                        case $kwh > 1000 && $kwh < 3000:
                            $annual_cost = $kwh * $row["price_2"];
                            break;
                        case $kwh > 3000:
                            $annual_cost = $kwh * $row["price_3"];
                            break;
                    }

                    // sort annual_cost

                    ?>
                    <tr>
                        <td><?php echo $row["provider"] ?></td>
                        <td><?php echo $row["information"] ?></td>
                        <td>€ <?php echo number_format(($annual_cost/100), 2); ?></td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
        <?php
    }
    ?>
</div>
<script>
    function sortTable(n) {
        var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
        table = document.getElementById("myTable1");
        switching = true;
        // Set the sorting direction to ascending:
        dir = "asc";
        /* Make a loop that will continue until
        no switching has been done: */
        while (switching) {
            // Start by saying: no switching is done:
            switching = false;
            rows = table.rows;
            /* Loop through all table rows (except the
            first, which contains table headers): */
            for (i = 1; i < (rows.length - 1); i++) {
                // Start by saying there should be no switching:
                shouldSwitch = false;
                /* Get the two elements you want to compare,
                one from current row and one from the next: */
                x = rows[i].getElementsByTagName("TD")[n];
                y = rows[i + 1].getElementsByTagName("TD")[n];
                /* Check if the two rows should switch place,
                based on the direction, asc or desc: */
                if (dir == "asc") {
                    if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                } else if (dir == "desc") {
                    if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                        // If so, mark as a switch and break the loop:
                        shouldSwitch = true;
                        break;
                    }
                }
            }
            if (shouldSwitch) {
                /* If a switch has been marked, make the switch
                and mark that a switch has been done: */
                rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
                switching = true;
                // Each time a switch is done, increase this count by 1:
                switchcount ++;
            } else {
                /* If no switching has been done AND the direction is "asc",
                set the direction to "desc" and run the while loop again. */
                if (switchcount == 0 && dir == "asc") {
                    dir = "desc";
                    switching = true;
                }
            }
        }
    }
</script>
</body>
</html>