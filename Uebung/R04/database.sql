create table students(
                         id integer primary key auto_increment,
                         name varchar(255) not null,
                         present varchar(5) not null
)ENGINE=InnoDB;

insert into students (name, present) VALUE ('Robin Adamek', 'True');
insert into students (name, present) VALUE ('Emanuel Ardelean', 'True');
insert into students (name, present) VALUE ('Johanna Bock', 'True');
insert into students (name, present) VALUE ('Simon Engelberger-Semper', 'True');
insert into students (name, present) VALUE ('Oliver Fahrnik', 'True');
insert into students (name, present) VALUE ('Dominik Ferfecky', 'True');
insert into students (name, present) VALUE ('Nico Fichtinger', 'True');
insert into students (name, present) VALUE ('David Kaufmann', 'True');
insert into students (name, present) VALUE ('Alexander Kern', 'True');
insert into students (name, present) VALUE ('Florian Marek', 'True');
insert into students (name, present) VALUE ('Julian Pronhagl', 'True');
insert into students (name, present) VALUE ('Julia Schenk', 'True');
insert into students (name, present) VALUE ('Armin Siegmeth', 'True');
insert into students (name, present) VALUE ('Johannes Spindelberger', 'True');
insert into students (name, present) VALUE ('Dennis Strupp', 'True');
insert into students (name, present) VALUE ('Gabriel Zeller', 'True');