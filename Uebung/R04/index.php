<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css">
    <link rel="icon" href="https://icon-library.com/images/random-icon-generator/random-icon-generator-7.jpg">
    <title>Randomizer</title>
</head>
<body style="min-height: 100vh; margin: 0; display: flex; flex-direction:column;">
<?php
# ------------------------------------------------
# Connect to Database
# ------------------------------------------------
$user = "htl";
$pw = "insy";
$dbname = "htl_students";
try {
    $conn = new PDO("mysql:host=localhost;dbname=$dbname", $user, $pw);
    // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM students";
    // $conn->exec($sql);
} catch (PDOException $e) {
    echo "<br>" . $e->getMessage();
}
?>
<div class="container">
    <div class="row">
        <h1 class="my-3">5 BHITM
            <button id="dice-btn" onclick="myFunction()" class="btn btn-lg btn-danger float-end me-3">
                <i class="bi bi-dice-2-fill"></i> Würfeln
            </button>
        </h1>
        <h4>Schüler:</h4>
        <?php
        foreach ($conn->query($sql) as $row) { ?>
            <div class="col-lg-3 col-sm-6 my-2">
                <div id="<?php echo $row['id'] ?>" class="card text-center" style="height: 90px">
                    <div class="card-body">
                        <?php echo $row['name'] ?><br>
                        <?php
                        if ($row['present'] == "True") {
                            echo "<i id=" . $row['id'] . " class='bi bi-check-circle-fill text-success'></i>";
                        } else {
                            echo "<i id=" . $row['id'] . " class='bi bi-x-circle-fill text-danger'></i>";
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<article style="flex: 1;"></article>
<footer class="text-center text-white" style="background-color: #f1f1f1;" id="foot">
    <div class="text-center text-dark p-3" style="background-color: rgba(0, 0, 0, 0.2);">
        © Zeller Gabriel, 5BHITM 2022/23
    </div>
</footer>
<div class="modal" id="myModal" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="lead">Wer ist zur Stundenwiederholung dran?</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body mb-3">
                <h6 class="display-4 text-center" id="student_name"></h6>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $(".bi-check-circle-fill, .bi-x-circle-fill").click(function () {
            let checkbox = $(this);
            $.ajax({
                url: 'index.php',
                success: function () {
                    let new_class = checkbox.attr('class');
                    if (new_class === "bi bi-check-circle-fill text-success") {
                        new_class = 'bi bi-x-circle-fill text-danger';
                    } else {
                        new_class = 'bi bi-check-circle-fill text-success';
                    }
                    checkbox.attr('class', new_class);
                },
            })
        });
    });

    function myFunction() {
        $(".card").addClass("shake shake-constant");
        $("#dice-btn").prop("disabled", true);
        setTimeout(chooseStudent, 3000);
    }

    function chooseStudent() {
        $(".card").removeClass("shake shake-constant");
        $("#dice-btn").prop("disabled", false);
        let available_ids = [];

        $(".bi-check-circle-fill").each(function () {
            let current_id = $(this).attr('id');
            available_ids.push(current_id);
        });
        let id = available_ids[Math.floor(Math.random() * available_ids.length)];

        $.ajax({
            type: 'GET',
            url: 'index.php',
            data: {
                student_id: id
            },
            success: function () {
                let div_value = $('#'+id).text();
                let student_name = div_value.trim();
                if (id == null) {
                    student_name = "NIEMAND, weil alle abwesend sind."
                }
                $('#student_name').text(student_name);
            }
        });
        $("#myModal").modal('show');
    }
</script>
</body>
</html>